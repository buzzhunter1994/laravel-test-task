const messages = {
    required: 'Field is required',
    email: 'Field must be a valid email',
    phone: 'Field must be a valid phone',
}

const required = v => !!v || messages.required

export const rules = {
    required,
    email: [
        required,
        v => /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(v) || messages.email
    ],
    phone: [
        required,
        v => window.intlTelInputUtils.isValidNumber(v) || messages.phone
    ],
}

export const validateValue = (value, rules) => rules.map((rule) => rule(value)).filter((e) => e !== true)
