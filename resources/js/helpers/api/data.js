import api from './index'

export const getCountries = () => {
    return new Promise((resolve, reject) => {
        api.get('countries').then(resolve).catch(e => reject(e))
    })
}
