import api from './index'
import { errorHandler } from './errors'

export const signup = (form) => {
    return new Promise((resolve, reject) => {
        api.post('signup', form).then(resolve).catch(e => reject(errorHandler(e)))
    })
}
