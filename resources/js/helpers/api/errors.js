export const errorHandler = (e) => {
    if (e.response && e.response.status === 422) {
        return {errors: e.response.data.errors}
    } else if (e.response) {
        return {message: e.response.data.message}
    }
}
