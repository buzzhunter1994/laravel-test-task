import './bootstrap'

import Vue from 'vue'
import vuetify from './vuetify'
import SignUp from './components/SignUp.vue'

window.Vue = Vue

Vue.component('sign-up', SignUp)

const app = new Vue({
    vuetify,
    el: '#app',
})
