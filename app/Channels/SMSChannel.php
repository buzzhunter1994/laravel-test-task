<?php declare(strict_types=1);

namespace App\Channels;

use App\Services\InforuSMSService;
use App\Models\Interfaces\CanSendSMSInterface;
use App\Notifications\Interfaces\ShouldSendSMSInterface;

class SMSChannel
{
    public function send(CanSendSMSInterface $notifiable, ShouldSendSMSInterface $notification): void
    {
        $message = $notification->toSMS($notifiable);

        if (!config('external-apis.inforu.service_enabled') || empty($message)) return;

        $smsService = new InforuSMSService();
        $smsService->sendMessage($notifiable->getNumber(), $message);
    }
}
