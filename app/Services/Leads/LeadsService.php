<?php declare(strict_types=1);

namespace App\Services\Leads;

use App\DTO\LeadDTO;
use App\Models\User;
use App\Repositories\Leads\LeadsRepository;

class LeadsService
{
    public function __construct(
        private LeadsRepository $leadsRepository
    )
    {
    }

    public function store(LeadDTO $leadDTO): User
    {
        return $this->leadsRepository->store($leadDTO);
    }
}
