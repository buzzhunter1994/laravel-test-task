<?php declare(strict_types=1);

namespace App\Services\Countries;

use Illuminate\Support\Facades\Storage;

class CountriesService implements CountriesServiceInterface
{
    public function getCountries(): array
    {
        return json_decode(Storage::get('public/countries.json'), true);
    }
}
