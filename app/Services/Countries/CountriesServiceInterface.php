<?php declare(strict_types=1);

namespace App\Services\Countries;

interface CountriesServiceInterface
{
    public function getCountries(): array;
}
