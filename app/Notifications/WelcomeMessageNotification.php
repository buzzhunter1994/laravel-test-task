<?php declare(strict_types=1);

namespace App\Notifications;

use App\Models\User;
use App\Channels\SMSChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use App\Models\Interfaces\CanSendSMSInterface;
use Illuminate\Notifications\Messages\MailMessage;
use App\Notifications\Interfaces\ShouldSendSMSInterface;

class WelcomeMessageNotification extends Notification implements ShouldSendSMSInterface
{
    use Queueable;

    public function via(): array
    {
        return ['mail', SMSChannel::class];
    }

    public function toMail(User $notifiable): MailMessage
    {
        return (new MailMessage)
            ->greeting("Hello, {$notifiable->name}")
            ->line('Thank you for using our application!');
    }

    public function toSMS(CanSendSMSInterface $notifiable): string
    {
        return view('notifications.sms.index', compact('notifiable'))->render();
    }
}
