<?php declare(strict_types=1);

namespace App\Notifications\Interfaces;

use App\Models\Interfaces\CanSendSMSInterface;

interface ShouldSendSMSInterface
{
    public function toSMS(CanSendSMSInterface $notifiable): string;
}
