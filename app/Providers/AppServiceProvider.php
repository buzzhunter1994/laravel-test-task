<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Countries\CountriesService;
use App\Services\Countries\CountriesServiceInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(CountriesServiceInterface::class, CountriesService::class);
    }
}
