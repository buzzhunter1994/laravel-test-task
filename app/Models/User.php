<?php declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\Models\Interfaces\CanSendSMSInterface;
use App\Notifications\WelcomeMessageNotification;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property $name
 * @property PhoneBook $phonesBook
 */
class User extends Model implements CanSendSMSInterface
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
    ];

    public function phonesBook(): HasOne
    {
        return $this->hasOne(PhoneBook::class);
    }

    public function countries(): HasOne
    {
        return $this->hasOne(UserCountry::class);
    }

    public function sendWelcomeMessageNotification()
    {
        $this->notify(new WelcomeMessageNotification());
    }

    public function getNumber(): string
    {
        return $this->phonesBook->number;
    }
}
