<?php declare(strict_types=1);

namespace App\Models\Interfaces;

interface CanSendSMSInterface
{
    public function getNumber(): string;
}
