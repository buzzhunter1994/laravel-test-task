<?php declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property $number
 */
class PhoneBook extends Model
{
    use HasFactory, Notifiable;

    protected $table = 'phone_book';
    public $timestamps = false;
    protected $fillable = [
        'number',
    ];
}
