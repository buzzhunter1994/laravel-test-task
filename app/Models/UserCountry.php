<?php declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserCountry extends Model
{
    use HasFactory, Notifiable;

    protected $fillable = [
        'name',
    ];

    public $timestamps = false;
}
