<?php declare(strict_types=1);

namespace App\DTO;

use Spatie\DataTransferObject\Attributes\MapFrom;
use Spatie\DataTransferObject\DataTransferObject;

class LeadDTO extends DataTransferObject
{
    public string $name;
    public string $email;
    public string $phone;

    #[MapFrom('country.name')]
    public string $country;
}
