<?php declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use App\Services\Countries\CountriesServiceInterface;

class CountriesController extends Controller
{
    public function index(CountriesServiceInterface $countriesService): JsonResponse
    {
        $countries = $countriesService->getCountries();

        return response()->json($countries);
    }
}
