<?php declare(strict_types=1);

namespace App\Http\Controllers;

use App\DTO\LeadDTO;
use App\Events\Registered;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\SignupRequest;
use App\Services\Leads\LeadsService;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

class SignupController extends Controller
{
    public function store(SignupRequest $request, LeadsService $leadsService): JsonResponse
    {
        try {
            $leadDTO = new LeadDTO($request->validated());

            $lead = $leadsService->store($leadDTO);

            event(new Registered($lead));

            return response()->json(['message' => 'Thank you for reaching out']);
        } catch (UnknownProperties $exception) {
            Log::error($exception->getMessage());
            return response()->json(['message' => 'Ooops! Something went wrong'], 400);
        }
    }
}
