<?php declare(strict_types=1);

namespace App\Repositories\Leads;

use App\DTO\LeadDTO;
use App\Models\User;

class LeadsRepository
{
    public function store(LeadDTO $leadDTO): User
    {
        $lead = new User($leadDTO->all());

        $lead->save();

        $lead->countries()->create(['name' => $leadDTO->country]);
        $lead->phonesBook()->create(['number' => $leadDTO->phone]);

        return $lead;
    }
}
