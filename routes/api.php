<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SignupController;
use App\Http\Controllers\CountriesController;

Route::get('countries', [CountriesController::class, 'index']);

Route::post('signup', [SignupController::class, 'store']);
