# Laravel Test Task

Get started

```git clone https://gitlab.com/buzzhunter1994/laravel-test-task.git```

```cd ./laravel-test-task && cp .env.example .env```

```touch database/db.sqlite ```

Fill DB_DATABASE in .env with the absolute path of database/db.sqlite

also, for proper sending welcome message you need to adjust MAIL_* and INFORU_* variables 

```composer install```

```npm install && npm run dev```

```php artisan key:generate && php artisan migrate```

```php artisan serve```
